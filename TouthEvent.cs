﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouthEvent : MonoBehaviour
{
    //观察目标  
    public Transform Target;
    //观察距离  
    public float Distance = 2F;
    //距离最值  
    public float MaxDistance = 5F;
    public float MinDistance = 0.5F;

    //缩放速率  
    private float ZoomSpeed = 0.15F;

    //旋转速度  
    private float SpeedX = 240;
    private float SpeedY = 120;
    //角度限制  
    private float MinLimitY = 5;
    private float MaxLimitY = 180;

    //旋转角度  
    private float mX = 0;
    private float mY = 0;

    //当前手势  
    private Vector2 mPos;
    //是否是两个手指
    private bool isTowFinger;
    private Touch oldTouch1;  //上次触摸点1(手指1)  
    private Touch oldTouch2;  //上次触摸点2(手指2)  

    void Start()
    {
        Distance = Vector3.Distance(transform.position, Target.position);
        //允许多点触控  
        Input.multiTouchEnabled = true;
        //初始化旋转  
        mX = Target.eulerAngles.x;
        mY = Target.eulerAngles.y;
    }
    private void TouchEvent1()
    {
        //没有mubiao   
        if (!Target) return;
        //没有触摸  
        if (Input.touchCount <= 0) { return; }

        //单点触摸， 旋转  
        if (1 == Input.touchCount)
        {
            if (!isTowFinger)
            {
                Ray ray;
                RaycastHit hit;
                // 生成射线  
                ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                if (Physics.Raycast(ray, out hit, Mathf.Infinity))
                {
                    //手指处于移动状态  
                    if (Input.touches[0].phase == TouchPhase.Moved)
                    {
                        //选择物体
                        if (hit.transform.name == Target.name)
                        {
                            Touch touch = Input.GetTouch(0);
                            Vector2 deltaPos = touch.deltaPosition;
                            Target.transform.Rotate(Vector3.down * deltaPos.x, Space.World);
                            Target.transform.Rotate(Vector3.right * deltaPos.y, Space.World);
                        }
                    }

                }
                else
                {
                    //旋转场景
                    mX += Input.GetAxis("Mouse X") * SpeedX * 0.02F;
                    mY -= Input.GetAxis("Mouse X") * SpeedY * 0.02F;
                    mY = ClampAngle(mY, MinLimitY, MaxLimitY);
                    //计算相机的角度和位置  
                    transform.rotation = Quaternion.Euler(new Vector3(mY, mX, 0));
                    transform.position = transform.rotation * new Vector3(Target.position.x, Target.position.y, -Distance) + Target.position;
                    //transform.position = transform.rotation * new Vector3(0, 0, -Distance) + Target.position;
                }
            }
            if (Input.touches[0].phase == TouchPhase.Ended)
            {
                isTowFinger = false;
            }
        }
        //多点触控  
        if (Input.touchCount > 1)
        {
            Ray ray;
            RaycastHit hit;
            // 生成射线  
            ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
            //多点触摸, 放大缩小  
            Touch newTouch1 = Input.GetTouch(0);
            Touch newTouch2 = Input.GetTouch(1);

            //第2点刚开始接触屏幕, 只记录，不做处理  
            if (newTouch2.phase == TouchPhase.Began)
            {
                oldTouch2 = newTouch2;
                oldTouch1 = newTouch1;
                return;
            }

            //计算老的两点距离和新的两点间距离，变大要放大模型，变小要缩放模型  
            float oldDistance = Vector2.Distance(oldTouch1.position, oldTouch2.position);
            float newDistance = Vector2.Distance(newTouch1.position, newTouch2.position);

            //两个距离之差，为正表示放大手势， 为负表示缩小手势  
            float offset = newDistance - oldDistance;

            //放大因子， 一个像素按 0.01倍来算(100可调整)  
            float scaleFactor = offset / 100f;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.transform.name == "CubeObj")
                {
                    Vector3 localScale = Target.transform.localScale;
                    Vector3 scale = new Vector3(localScale.x + scaleFactor,
                                                localScale.y + scaleFactor,
                                                localScale.z + scaleFactor);
                    //最小缩放到 0.3 倍  
                    if (scale.x > 0.1f && scale.y > 0.1f && scale.z > 0.1f)
                    {
                        Target.transform.localScale = scale;
                    }
                    //记住最新的触摸点，下次使用  
                    oldTouch1 = newTouch1;
                    oldTouch2 = newTouch2;
                }
            }
            else
            {
                //计算移动方向  
                Vector2 mDir = Input.touches[1].position - Input.touches[0].position;
                //根据向量的大小判断当前手势是放大还是缩小  
                if (mDir.sqrMagnitude > mPos.sqrMagnitude)
                {
                    Distance -= ZoomSpeed;
                }
                else
                {
                    Distance += ZoomSpeed;
                }
                //限制距离  
                Distance = Mathf.Clamp(Distance, MinDistance, MaxDistance);
                //更新当前手势  
                mPos = mDir;
                //计算相机的角度和位置  
                transform.rotation = Quaternion.Euler(new Vector3(mY, mX, 0));
                transform.position = transform.rotation * new Vector3(0, 0, -Distance) + Target.position;
            }
        }
    }
        private void TouchEvent()
    {
        if (!Target) return;


        //单点触控  
        if (Input.touchCount == 1)
        {
            //手指处于移动状态  
            if (Input.touches[0].phase == TouchPhase.Moved)
            {
                mX += Input.GetAxis("Mouse X") * SpeedX * 0.02F;
                mY -= Input.GetAxis("Mouse X") * SpeedY * 0.02F;
                mY = ClampAngle(mY, MinLimitY, MaxLimitY);
            }
        }

        //多点触控  
        if (Input.touchCount > 1)
        {
            //两只手指都处于移动状态  
            if (Input.touches[0].phase == TouchPhase.Moved || Input.touches[1].phase == TouchPhase.Moved)
            {
                //计算移动方向  
                Vector2 mDir = Input.touches[1].position - Input.touches[0].position;
                //根据向量的大小判断当前手势是放大还是缩小  
                if (mDir.sqrMagnitude > mPos.sqrMagnitude)
                {
                    Distance -= ZoomSpeed;
                }
                else
                {
                    Distance += ZoomSpeed;
                }
                //限制距离  
                Distance = Mathf.Clamp(Distance, MinDistance, MaxDistance);
                //更新当前手势  
                mPos = mDir;
            }
        }

        //计算相机的角度和位置  
        transform.rotation = Quaternion.Euler(new Vector3(mY, mX, 0));
        transform.position = transform.rotation * new Vector3(0, 0, -Distance) + Target.position;

    }
    void Update()
    {
        TouchEvent1();
    }

    //角度限制  
    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360) angle += 360;
        if (angle > 360) angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }
}
